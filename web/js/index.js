var mysql = require('mysql');
var express = require('express');
var app= express();
var port = process.env.PORT || 8005;
var responseStr= "MYSQL Data:";

app.get('/',function(req, res){
   var mysqlhost  = process.env.MYSQL_HOST  || '192.168.175.133';
   var mysqlport  = process.env.MYSQL_PORT  || '3306';
   var mysqluser  = process.env.MYSQL_USER  || 'root';
   var mysqlupass = process.env.MYSQL_PASS  || 'root';
   var mysqldb    = process.env.MYSQL_DB    || 'sistemas';
   var connectionOptions={
       host: mysqlhost,
       port: mysqlport,
       user: mysqluser,
       password: mysqlupass,
       database: mysqldb
   };
   console.log('MSQL_Connection config:');
   console.log(connectionOptions);
   var connection = mysql.createConnection(connectionOptions);
   connection.connect();
   connection.query('SELECT * FROM estudiante',function(error,results,fields){
    if(error) throw error;
    responseStr='';
    results.forEach(function(data){
        responseStr+=data.nombre+ ' : ';
        console.log(data);
    });
    if(responseStr.length==0)
        responseStr='Norecords found';
        console.log(responseStr);

        res.status(200).send(responseStr)
   });
   connection.end();

});
app.listen(port, function(){
    console.log("sample mysql app listening on port"+port);
});
