const express=require('express');
const app= express();
const morgan = require('morgan');
var port = process.env.PORT || 8005;
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(session({
  secret: 'faztmysqlnodemysql',
  resave: false,
  saveUninitialized: false,
  store: new MySQLStore(database)
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(validator());

app.use(require('./js/index.js'));

app.listen(port,()=>{
    console.log('servidor iniciado');
});