FROM debian:latest
RUN apt-get update -y
RUN apt-get install nodejs npm -y
COPY web/ /app/
WORKDIR /app
RUN npm install express 
RUN npm install mysql
EXPOSE 8005
CMD node index.js
